from core.contrib import app, db
from flask import render_template, \
    Blueprint, \
    redirect, \
    flash, \
    abort
from pages.forms import LoginForm, RegisterForm, QuestionForm, AskForm
from users.models import User, Question, Ask
from users.routes import load_user
import hashlib
from flask_login import login_user, current_user, logout_user
import datetime

site_index = Blueprint('index', __name__, template_folder='templates')

@app.route('/', methods=["GET", "POST"])
def index():
    question_form = QuestionForm()
    questions = Question.query.order_by(Question.pub_date.desc()).all()

    if question_form.validate_on_submit():
        
        text = question_form.text.data
        question = Question(text=text,
                            user_id=current_user.id,
                            pub_date = datetime.datetime.now())

        db.session.add(question)
        db.session.flush()
        db.session.refresh(question)
        question_id = question.id
        db.session.commit()

        return redirect('/question/%s' % question_id)

    return render_template('index.html',
                           question_form=question_form,
                           questions=questions)

@app.route('/register', methods=["GET", "POST"])
def register():

    if current_user.is_authenticated():
        return redirect('/')

    register_form = RegisterForm()

    if register_form.validate_on_submit():
        email = register_form.email.data
        name = register_form.name.data
        password = register_form.password.data
        tag_name = register_form.tag_name.data

        hash_type = hashlib.sha224()
        hash_type.update(password)
        hash_password = hash_type.hexdigest()


        user = User(email=email,
                    name=name,
                    password=hash_password,
                    reg_date = datetime.datetime.now(),
                    tag_name = tag_name)
        db.session.add(user)
        db.session.commit()

        return redirect('/login')

    return render_template('register.html', register_form=register_form)

@app.route('/question/<question_id>', methods=["GET", "POST"])
def question_detail(question_id):
    ask_form = AskForm()
    question = Question.query.filter_by(id=question_id)
    asks = Ask.query.filter_by(question_id=question_id)
    db.session.commit()

    for item in question:
        user_id = item.user_id

    try:
        user = User.query.filter_by(id=user_id)

    except UnboundLocalError:
        abort(404)


    if ask_form.validate_on_submit():
        ask_text = ask_form.text.data
        new_ask = Ask(text=ask_text,
                      question_id=question_id,
                      user_id=current_user.id,
                      ask_date = datetime.datetime.now())
        db.session.add(new_ask)
        db.session.commit()

        return render_template('replies.html',
                               ask_form=ask_form,
                               question=question,
                               asks=asks,
                               user=user)

    return render_template('replies.html',
                           ask_form=ask_form,
                           question=question,
                           asks=asks,
                           user=user)

@app.route('/login', methods=["GET", "POST"])
def login():

    if current_user.is_authenticated():

        return redirect('/')

    login_form = LoginForm()

    if login_form.validate_on_submit():

        get_password_hash = hashlib.sha224(login_form.password.data).hexdigest()  # SHA HASH
        get_user = User.query.filter_by(email=login_form.email.data)

        if get_user:

            for item in get_user:

                user_id=item.id
                user_password = item.password
                user = User.query.get(user_id)

                if user_password == get_password_hash:

                    user.authenticated = True
                    db.session.add(user)
                    db.session.commit()
                    login_user(user, remember=True)

                    return redirect('/')

        flash('Incorrect Login And Password', category='error')  # If error

    return render_template('login.html', login_form=login_form)


@app.route('/logout')
def logout():

    if current_user.is_authenticated():
        user_id = current_user.id
        user = User.query.get(user_id)
        user.authenticated = False
        db.session.add(user)
        db.session.commit()
        logout_user()
        flash('Succes logout, good bye!', category='logout')

        return redirect('/login')

    else:

        return redirect('/login')

@app.route('/profile/<user_id>')
def profile(user_id):
    user = User.query.filter_by(id=user_id)
    questions_count = Question.query.filter_by(user_id=user_id)
    asks_count = Ask.query.filter_by(user_id=user_id)

    return render_template('profile.html', user=user,
                           questions_count=questions_count,
                           asks_count=asks_count)




@app.errorhandler(404)
def not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def not_found(e):
    return render_template('500.html'), 500



