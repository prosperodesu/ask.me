from flask import Flask, app
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.gravatar import Gravatar
from redis import Redis

app = Flask(__name__)

db = SQLAlchemy(app)

login_manager = LoginManager()


gravatar = Gravatar(app,
                    size=50,
                    rating='x',
                    default='retro',
                    force_default=False,
                    force_lower=False,
                    use_ssl=False,
                    base_url=None)

redis = Redis()




