#Filters
from core import app

# Convert date
@app.template_filter('format_date')
def format_date(s):
    return s.strftime('%d %B %Y (%A) %H:%M:%S')