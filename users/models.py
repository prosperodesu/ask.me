from core.contrib import app, db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50))
    tag_name = db.Column(db.String(50))
    root = db.Column(db.Boolean, default=False)
    password = db.Column(db.String(200))
    reg_date = db.DateTime()
    authenticated = db.Column(db.Boolean, default=False)
    active = db.Column(db.Boolean, default=True)
    votes = [db.Column(db.String(200))]

    def __unicode__(self):
        return str(self.id)

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def is_root(self):
        return self.root

    def get_id(self):
        return self.id

class Ask(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(500))
    ask_date = db.DateTime()
    rating = db.Column(db.Integer(), default=0)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    question = db.relationship('Question', backref=db.backref('asks', lazy='dynamic'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('asks', lazy='dynamic'))

    def __unicode__(self):
        return self.text


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(500))
    pub_date = db.Column(db.DateTime())
    page_views = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('questions', lazy='dynamic'))

    def __unicode__(self):
        return self.text