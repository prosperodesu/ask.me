from flask.ext.wtf import Form
from wtforms.fields import TextField, StringField, PasswordField, BooleanField
from wtforms import validators, ValidationError
from users.models import User

class LoginForm(Form):

    email = StringField('Email',
                        validators=[validators.required(),
                                    validators.length(max=50)])
    password = PasswordField('Password',
                             validators=[validators.required(),
                                         validators.length(max=50)])

class RegisterForm(Form):

    def email_unique_check(form, field):
        email_exist = User.query.filter_by(email=field.data)
        for item in email_exist:
            if item.email:
                raise ValidationError('Email already register!')

    email = StringField('Email',
                        validators=[validators.required(),
                                             validators.email(message='Please enter correct email'),
                                             email_unique_check,
                                             validators.length(max=50)])
    name = StringField('Name',
                       validators=[validators.required(),
                                   validators.length(max=50)])

    tag_name = StringField('Tag',
                           validators=[validators.required(),
                           validators.length(max=50)])
    password = StringField('Password',
                           validators=[validators.required(),
                                       validators.length(min=6, max=50)])
    repeat_password = StringField('Repeat Password',
                                  validators=[validators.required(),
                                                                 validators.equal_to('password', message='Passwords must be Equal',)])

class QuestionForm(Form):
    text = StringField('Question', validators=[validators.required(),
                                               validators.length(max=400)])

class AskForm(Form):
    text = StringField('Ask', validators = [validators.required(),
                                            validators.length(max=400)])