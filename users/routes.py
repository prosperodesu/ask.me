from flask import request
from flask import jsonify
from core.contrib import app, login_manager, redis, db
from users.models import User, Ask
import time

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

@app.route('/vote', methods=['GET', 'POST'])
def voted():
    now = int(time.time())
    redis_instance = redis.pipeline()
    user_id = request.args.get("user_id")
    ask_id = request.args.get("ask_id")
    vote_action = request.args.get("action")
    get_ask = Ask.query.get(ask_id)

    if vote_action == 'unmark':
        check_mark_data = redis.get('vote_asks#%s_user#%s_action#%s' % (ask_id, user_id, 'mark'))

        if check_mark_data:
            get_ask.rating = int(get_ask.rating) - 1
            user_key = 'vote_asks#%s_user#%s_action#%s' % (ask_id, user_id, 'mark')
            redis_instance.delete(user_key)
            redis_instance.execute()
            db.session.commit()
            return jsonify(status = 'unmark')

    elif vote_action == 'mark':
        check_mark_data = redis.get('vote_asks#%s_user#%s_action#%s' % (ask_id, user_id, 'mark'))
        if check_mark_data == None:
            get_ask.rating = int(get_ask.rating) +  1
            user_key = 'vote_asks#%s_user#%s_action#%s' % (ask_id, user_id, 'mark')
            redis_instance.set(user_key, now)
            redis_instance.execute()
            db.session.commit()
            return jsonify(status = 'mark')

    elif vote_action == 'check_vote':
        vote_check = redis.get('vote_asks#%s_user#%s_action#%s' % (ask_id, user_id, 'mark'))

        if vote_check == None:
            return jsonify(status='not_voted_by')

        if vote_check:
            return jsonify(status='is_voted_by')


