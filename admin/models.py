from flask.ext.admin.contrib.sqla import ModelView
class UserModel(ModelView):
    list_display = ('id', 'email', 'name', 'password', 'reg_date')

    '''Password view in admin(sha224: 3b23452*******)'''
    column_formatters = dict(password=lambda v, c, m, p: 'sha224: ' + m.password[:6]+ '*' * 58)



class QuestionModel(ModelView):
    list_display = ('id', 'text', 'pub_date')

class AskModel(ModelView):
    list_display = ('id', 'text', 'ask_date', 'rating', 'pub_date')
