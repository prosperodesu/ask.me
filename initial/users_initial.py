#Initial data for Create Users

import hashlib

users_data = {'Evgeniy': {
                'id': 1,
                'email': 'kolabrod@gmail.com',
                'name': 'Evgeniy Cirnov',
                'tag_name': 'tachana',
                'password': hashlib.sha224('12345678').hexdigest(),
                'is_root': True

            },
              'Anna' : {
                  'id': 2,
                  'email': 'uzumake007@yandex.ru',
                  'name': 'Ann Tachibana',
                  'tag_name': 'sugnum',
                  'password': hashlib.sha224('ekza2e3').hexdigest(),
                  'is_root': False

            },
              'Yuriy': {
                  'id': 3,
                  'email': 'obivan784@qip.ru',
                  'name': 'Yuriy Horoshevskiy',
                  'tag_name': 'kiry',
                  'password': hashlib.sha224('kmz41').hexdigest(),
                  'is_root' : False
              },

              'Viktorya': {
                  'id': 4,
                  'email': 'hector007@pochta.ru',
                  'name': 'Viktorya Rimar',
                  'tag_name': 'somebody',
                  'password': hashlib.sha224('kmz41').hexdigest(),
                  'is_root' : False
              }
        }