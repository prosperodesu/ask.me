from core import app, db, login_manager, redis
import logging
from logging import FileHandler
from settings import DEBUG, STATIC
from pages.routes import site_index
from flask_triangle import Triangle


if DEBUG == True:
    file_handler = FileHandler("debug.log","a") # Write Logs
    file_handler.setLevel(logging.WARNING)
    app.logger.addHandler(file_handler)
    from werkzeug import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
      '/': STATIC # Handle Static
    })


app.register_blueprint(site_index)

login_manager.init_app(app)
Triangle(app)