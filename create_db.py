from core.contrib import db
from users.models import User, Question, Ask
from initial.users_initial import users_data
from initial.questions_initial import questions_data
from initial.asks_initial import asks_data
import hashlib
import datetime

db.create_all()

class Initial():
    def __init__(self):
        self.users_data = users_data
        self.questions_data = questions_data
        self.asks_data = asks_data

    def create_users(self):
        print '\t' + '-' * 58 + 'INITIAL USERS' + '-' *58 + '\n'
        for item, values in self.users_data.items():
            print '!Create Initial for %s: 00%s' % (item, values['id'])
            print '\t -email:%s\n\t -password:%s\n\t -name:%s\n\t -tag: %s\n\t' %(values['email'],
                                                                     values['name'],
                                                                     values['password'],
                                                                     values['tag_name'])
            new_users = User(email=values['email'],
            name=values['name'],
            password=values['password'],
            tag_name = values['tag_name'],
            is_root = values['is_root'],
            reg_date = datetime.datetime.now())
            db.session.add(new_users)

    def create_questions(self):
        print '\t' + '-' * 58 + 'INITIAL QUESTIONS' + '-' *58 + '\n'
        for item, values in self.questions_data.items():
            print '!Create Initial for %s: 00%s' % (item, values['id'])
            print '\t -text:%s\n\t' % (values['text'])
            new_questions = Question(text=values['text'],
                                     user_id=values['user_id'],
                                     pub_date = datetime.datetime.now())
            db.session.add(new_questions)

    def create_asks(self):
        print '\t' + '-' * 58 + 'INITIAL ASKS' + '-' *58 + '\n'
        for item, values in self.asks_data.items():
            print '!Create Initial for %s: 00%s' % (item, values['id'])
            print '\t -text:%s\n\t' % (values['text'])
            new_asks = Ask(text=values['text'],
                           question_id=values['question_id'],
                           rating=values['rating'],
                           user_id=values['user_id'],
                           ask_date = datetime.datetime.now())
            db.session.add(new_asks)



init_db = Initial()
init_db.create_users()
init_db.create_questions()
init_db.create_asks()
db.session.commit()