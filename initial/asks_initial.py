#Initial data for Asks

asks_data = {'First Ask': {
    'id': 1,
    'text': 'With the exception of some hotels, some high-end (tourist-focused)?',
    'pub_date': '',
    'question_id': 1,
    'rating': 12,
    'user_id': 1,


}, 'Second Ask': {
    'id': 2,
    'text': 'Lonely Planet has a practical section on money costs in Istanbul:',
    'pub_date': '',
    'rating': 4,
    'question_id': 1,
    'user_id': 3,

}, 'Third Ask': {
    'id': 3,
    'text': 'I linked the entire article in case interested in reading the rest.',
    'pub_date': '',
    'rating': 8,
    'question_id': 1,
    'user_id': 1,

}, 'Fourth Ask': {
    'id': 4,
    'text': 'I guess for my 3rd question from the common sense point of view these functions should be thread-safe.',
    'pub_date': '',
    'rating': 9,
    'question_id': 2,
    'user_id': 2,

}, 'Fifth Ask': {
    'id': 5,
    'text': 'The OS designers, however, do know how to avoid it. In reality',
    'pub_date': '',
    'rating': 4,
    'question_id': 2,
    'user_id': 3,

}, 'Sixth Ask': {
    'id': 6,
    'text': 'Thanks a lot, this is very useful. I guess i was thinking in terms of old pthreads implementation.',
    'pub_date': '',
    'rating': 6,
    'question_id': 2,
    'user_id': 4,

}, 'Seventh Ask': {
    'id': 7,
    'text': 'Want to define first level of crawl in one method crawl_page and the second level of crawl in another?',
    'pub_date': '',
    'rating': 9,
    'question_id': 3,
    'user_id': 1,

}, 'Eighth Ask': {
    'id': 8,
    'text': 'Finally need to return the items with (name,url,age,sex,link) in a single item. ',
    'pub_date': '',
    'rating': 21,
    'question_id': 3,
    'user_id': 2,

}, 'Nineth Ask': {
    'id': 9,
    'text': 'What is your question?',
    'pub_date': '',
    'rating': 13,
    'question_id': 3,
    'user_id': 2,
}}