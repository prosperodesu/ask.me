from core.contrib import app
from core.contrib import db
from admin.contrib import admin
from admin.models import UserModel
from users.models import User, Question, Ask
import os
from flask.ext.admin.contrib import sqla
from pages.filters import format_date

# Run settings
autoreload = True
port = 5000
threaded=True

salt = 'tr/:;<=>?@[//]^_`/ABCDEFGabcdef/'

# App settings
app.secret_key = 'sa2342eda234214'
CSRF_ENABLED = True

#Debug settings
DEBUG = False

admin.add_view(UserModel(User, db.session))
admin.add_view(sqla.ModelView(Question, db.session))
admin.add_view(sqla.ModelView(Ask, db.session))


STATIC = os.path.join(os.path.dirname(__file__), 'static')

app.config['REDIS_URL'] = "redis://:password@54.93.193.22:6379/0"



#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/eda324s4132dfdp02j2.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://ecsv2:fiftcent@54.93.193.22/ecsv2'

#Expire Vote
app.config['VOTE_EXPIRE_MINUTES' ]= 1600

#Register Filters
app.jinja_env.filters['format_date'] = format_date